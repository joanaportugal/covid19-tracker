import React from "react";

import "./style.css";

const Header = () => {
  return (
    <header>
      <h1>Portugal Covid-19 Status</h1>
    </header>
  );
};

export default Header;
