import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import axios from "axios";

import format_number from "../../utils/format_number";

import "./style.css";

const Card = ({ title, number }) => {
  const getColors = (title) => {
    if (title === "Cases") {
      return {
        bg: "#cdcdcd",
        b: "1px solid grey",
      };
    } else if (title === "Deaths") {
      return {
        bg: "#ffcccb",
        b: "1px solid red",
      };
    }
    return { bg: "lightgreen", b: "1px solid green" };
  };

  const colors = getColors(title);

  return (
    <article
      style={{ background: colors.bg, border: colors.b }}
      className="card"
    >
      <p>{title}</p>
      <p>{number ? format_number(number) : "Loading..."}</p>
    </article>
  );
};

const Info = ({ data }) => {
  const pStyle = { fontSize: "1.2rem", lineHeight: "1.8rem" };
  const spanStyle = { fontWeight: "bold" };

  return (
    <div>
      <p style={pStyle}>
        <span style={spanStyle}>Last updated: </span>
        {data.data}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases: </span>
        {format_number(data.confirmados)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>New confirmed cases: </span>
        {format_number(data.confirmados_novos)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Recovered cases: </span>
        {format_number(data.recuperados)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases: </span>
        {format_number(data.obitos)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Hospitalized cases: </span>
        {format_number(data.internados + data.internados_uci)}
      </p>
      <br />

      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (0-9): </span>
        {format_number(data.confirmados_0_9_f + data.confirmados_0_9_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (10-19): </span>
        {format_number(data.confirmados_10_19_f + data.confirmados_10_19_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (20-29): </span>
        {format_number(data.confirmados_20_29_f + data.confirmados_20_29_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (30-39): </span>
        {format_number(data.confirmados_30_39_f + data.confirmados_30_39_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (40-49): </span>
        {format_number(data.confirmados_40_49_f + data.confirmados_40_49_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (50-59): </span>
        {format_number(data.confirmados_50_59_f + data.confirmados_50_59_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (60-69): </span>
        {format_number(data.confirmados_60_69_f + data.confirmados_60_69_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (70-79): </span>
        {format_number(data.confirmados_70_79_f + data.confirmados_70_79_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Confirmed cases (80+): </span>
        {format_number(data.confirmados_80_plus_f + data.confirmados_80_plus_m)}
      </p>

      <br />

      <p style={pStyle}>
        <span style={spanStyle}>Death cases (0-9): </span>
        {format_number(data.obitos_0_9_f + data.obitos_0_9_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (10-19): </span>
        {format_number(data.obitos_10_19_f + data.obitos_10_19_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (20-29): </span>
        {format_number(data.obitos_20_29_f + data.obitos_20_29_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (30-39): </span>
        {format_number(data.obitos_30_39_f + data.obitos_30_39_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (40-49): </span>
        {format_number(data.obitos_40_49_f + data.obitos_40_49_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (50-59): </span>
        {format_number(data.obitos_50_59_f + data.obitos_50_59_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (60-69): </span>
        {format_number(data.obitos_60_69_f + data.obitos_60_69_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (70-79): </span>
        {format_number(data.obitos_70_79_f + data.obitos_70_79_m)}
      </p>
      <p style={pStyle}>
        <span style={spanStyle}>Death cases (80+): </span>
        {format_number(data.obitos_80_plus_f + data.obitos_80_plus_m)}
      </p>
    </div>
  );
};

const CountryStats = () => {
  const [allInfo, setAllInfo] = useState(undefined);
  const [modalIsOpen, setIsOpen] = useState(false);

  useEffect(() => {
    axios
      .get("https://covid19-api.vost.pt/Requests/get_last_update")
      .then((res) => res.data)
      .then((data) => setAllInfo(data));
  });

  const modelButtonStyle = {
    margin: "1rem 0",
    background: "white",
    border: "1px solid black",
    borderRadius: "0.5rem",
    fontSize: "1rem",
    padding: "0.5rem",
    outline: "none",
  };

  return (
    <div id="general_stats">
      <section>
        <Card
          title="Cases"
          number={allInfo ? allInfo.confirmados : undefined}
        />
        <Card
          title="Recovered"
          number={allInfo ? allInfo.recuperados : undefined}
        />
        <Card title="Deaths" number={allInfo ? allInfo.obitos : undefined} />
      </section>
      <button onClick={() => setIsOpen(true)}>See more</button>
      {allInfo && (
        <Modal isOpen={modalIsOpen}>
          <Info data={allInfo} />
          <button onClick={() => setIsOpen(false)} style={modelButtonStyle}>
            Close
          </button>
        </Modal>
      )}
    </div>
  );
};

export default CountryStats;
