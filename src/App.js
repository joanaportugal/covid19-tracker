import React from "react";

import Header from "./components/Header";
import CountryStats from "./components/CountryStats";

import "./style.css";

const App = () => {
  return (
    <React.Fragment>
      <Header />
      <CountryStats />
    </React.Fragment>
  );
};

export default App;
